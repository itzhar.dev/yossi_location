var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var app = express();
var ParseDashboard = require('parse-dashboard');

var options = { allowInsecureHTTP: false };
var dashboard = new ParseDashboard({
  "apps": [
    {
      "serverURL": "http://localhost:1337/parse",
      "appId": "CHOOSE_APP_ID",
      "masterKey": "CHOOSE_MASTER_KEY",
      "appName": "Location App"
    }
  ]
}, options);


 
var api = new ParseServer({
  databaseURI: 'mongodb://yossi:12345@ds239177.mlab.com:39177/yossi_elkrief', // Connection string for your MongoDB database
  cloud: 'cloud/main.js', // Absolute path to your Cloud Code
  appId: 'CHOOSE_APP_ID',
  masterKey: 'CHOOSE_MASTER_KEY', // Keep this key secret!
  fileKey: 'CHOOSE_FILE_KEY',
  serverURL: 'http://localhost:1337/parse' // Don't forget to change to https if needed
});
 
// Serve the Parse API on the /parse URL prefix
app.use('/parse', api);
 
// make the Parse Dashboard available at /dashboard
app.use('/dashboard', dashboard);

app.listen(1337, function() {
  console.log('parse-server-example running on port 1337.');
});